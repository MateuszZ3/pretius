package sum;

import java.util.ArrayList;
import java.util.List;

public class AmountsCollection {
	private List<Amount> amounts;
	
	public AmountsCollection() {
		amounts = new ArrayList<Amount>();
	}
	
	boolean addAmount(Amount a) {
		return amounts.add(a);
	}
	
	public String getSum() {
		Amount sum = new Amount(); 
		
		for (Amount a : amounts) {
			sum = sum.addition(a);
		}
		return sum.toString();
	}
}
