package sum;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Sum {

	public static void main(String[] args) {
		Data data = new Data();
		
		if (args.length == 0) {
			System.out.println("Brak argumentu ze sciezka do pliku.");
		} else {
			try {
				BufferedReader br = new BufferedReader(new FileReader(new File(args[0])));
				String line = br.readLine();
			    
				while (line != null) {
			    	data.addLine(line);
			    	line = br.readLine();
				} 
				br.close();
			    System.out.println(data.getSum());
			} catch (FileNotFoundException e1) {
	        	System.out.println("Brak pliku o podanej sciezce.");
	        } catch (IOException e2) {
	        	e2.printStackTrace();
	        }
		}

	}
}
