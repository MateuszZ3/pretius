package sum;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Data {
	private static final Pattern pattern = Pattern.compile("((@amount:)(\\d+)(,)(\\d+)(PLN))");
	private AmountsCollection amColl;
	
	public Data() {
		this.amColl = new AmountsCollection();
	}
	
	public void addLine(String line){
		Matcher matcher = pattern.matcher(line);
		
		while (matcher.find()) {
			amColl.addAmount(new Amount(matcher.group(3), matcher.group(5)));
		}
	}
	
	public String getSum() {
		return amColl.getSum();
	}
}