package sum;

public class Amount {
	private Long floor;
	private Long mantissa;
	
	public Amount(long floor, long mantissa) {
		Long f = floor;
		Long m = mantissa;
		
		while (m >= 100) {
			m -= 100;
			f += 1;
		}
		this.floor = f;
		this.mantissa = m;
	}
	
	public Amount() {
		this(0, 0);
	}
	
	public Amount(String floor, String mantissa) {
		this(Long.valueOf(floor).longValue(), Long.valueOf(mantissa).longValue());
	}
	
	public Amount addition(Amount a) {
		return new Amount((floor + a.getFloor()), (mantissa + a.getMantissa()));
	}
	
	@Override
	public String toString() {
		return floor.toString() + "," + mantissa.toString() + "PLN";
	}
	
	Long getFloor() {
		return floor;
	}
	
	Long getMantissa() {
		return mantissa;
	}
}
